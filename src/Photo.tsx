import React, { useState } from 'react';
import { Alert, Text, Image, View, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import axios from 'axios';

import RNFS from 'react-native-fs';

async function getLocalData() {
  // const allExtFilesDirs = await RNFS.getAllExternalFilesDirs();
  // console.log('🚧 🚧 🚧 : getLocalData -> allExtFilesDirs', allExtFilesDirs);

  // get a list of files and directories in the main bundle
  RNFS.readDir(RNFS.MainBundlePath) // On Android, use "RNFS.DocumentDirectoryPath" (MainBundlePath is not defined)
    .then((result) => {
      console.log('GOT RESULT', JSON.stringify(result, null, 2));

      // stat the first file
      return Promise.all([RNFS.stat(result[0].path), result[0].path]);
    })
    .then((statResult) => {
      if (statResult[0].isFile()) {
        // if we have a file, read it
        return RNFS.readFile(statResult[1], 'utf8');
      }

      return 'no file';
    })
    .then((contents) => {
      // log the file contents
      console.log(contents);
    })
    .catch((err) => {
      console.log(err.message, err.code);
    });
}

export default function Photo(props) {
  const [photo, setPhoto] = useState('https://res.cloudinary.com/duisareexpensive/image/upload/v1594765417/sample.jpg');

  const cloudinaryUpload = async (photo) => {
    console.log('🚧 🚧 🚧 Uploading to Cloudinary...');
    const formData = new FormData();
    formData.append('file', photo);
    formData.append('folder', 'random_folder_name');
    // formData.append('public_id', 'MyPublic_ID') // <-- add a photo name but the photo_id ends up being "folder_name/picture_name"
    formData.append('tags', 'qwerty'); // <-- add something to make getting gallery photos grouped easier?
    formData.append('upload_preset', 'duisreallysuck');
    formData.append('cloud_name', 'duisareexpensive');

    try {
      const { data } = await axios({
        method: 'post',
        url: 'https://api.cloudinary.com/v1_1/duisareexpensive/image/upload',
        data: formData,
      });

      if (data.error) throw data.error;

      console.log('🚧 🚧 🚧 : cloudinaryUpload -> data', JSON.stringify(data, null, 2));
      console.log('🚧 🚧 🚧 Upload complete');

      setPhoto(data.secure_url);
    } catch (error) {
      console.log('🚧 🚧 🚧 : cloudinaryUpload -> error', error);
      Alert.alert('An error occurred while uploading');
    }
  };

  const choosePhoto = () => {
    getLocalData();
    const options = {
      title: 'Select Photo',
      noData: true,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      // console.log('🚧 🚧 🚧 : selectPhotoTapped -> response', response); // Note: very large!
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('🚧 🚧 🚧 : selectPhotoTapped -> response.error', response.error);
      } else {
        const source = {
          uri: response.uri,
          type: response.type,
          name: response.fileName || 'fileName', // <-- this being 'null' was the problem all along!
        };
        // console.log('🚧 🚧 🚧 : selectPhotoTapped -> source', source);

        cloudinaryUpload(source);
      }
    });
  };

  return (
    <View>
      <View>
        <TouchableOpacity style={styles.uploadButton} onPress={choosePhoto}>
          <Text style={styles.uploadButtonText}>Choose Photo</Text>
        </TouchableOpacity>
      </View>
      <View>
        <Image source={{ uri: photo }} style={{ height: 200, width: 200 }} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  imageContainer: {
    backgroundColor: '#fe5b29',
    height: Dimensions.get('window').height,
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
  },
  uploadContainer: {
    backgroundColor: '#f6f5f8',
    borderTopLeftRadius: 45,
    borderTopRightRadius: 45,
    position: 'absolute',
    bottom: 0,
    width: Dimensions.get('window').width,
    height: 200,
  },
  uploadContainerTitle: {
    alignSelf: 'center',
    fontSize: 25,
    margin: 20,
  },
  uploadButton: {
    borderRadius: 16,
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 7,
      height: 5,
    },
    shadowOpacity: 1.58,
    shadowRadius: 9,
    elevation: 4,
    margin: 10,
    padding: 10,
    backgroundColor: '#fe5b29',
    width: Dimensions.get('window').width - 60,
    alignItems: 'center',
  },
  uploadButtonText: {
    color: '#f6f5f8',
    fontSize: 20,
  },
});
