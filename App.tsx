import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import * as SplashScreen from 'expo-splash-screen';

import Photo from './src/Photo';

export default function App() {
  // const [isReady, setIsReady] = useState(false);
  // const [isLoaded, setIsLoaded] = useState(false);

  // useEffect(() => {
  // 	// Stop the Splash Screen from being hidden.
  //   const showSplashScreen = async () => {
  //     await SplashScreen.preventAutoHideAsync();
  //   }
  //   showSplashScreen();
  //   // You can do additional data fetching here.
  //   // I have a function that fetches my user from Firebase
  //   // but I have left it out because it is kind of irrelevant
  //   // in this demo.
  //   setIsLoaded(true);
  // }, []);

  // useEffect(() => {
  // 	// Once our data is ready, hide the Splash Screen
  //   const hideSplashScreen = async () => {
  //     await SplashScreen.hideAsync();
  //   }

  //   if (isLoaded && isReady) hideSplashScreen();
  // }, [isReady])

  // if (!isReady) return null;
  return (
    <View style={styles.container}>
      <Photo />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
